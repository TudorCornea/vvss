package agenda.model.validator;

public class Validate {

    public boolean validName(String str)
    {

        String[] s = str.split("[\\p{Punct}\\s]+");
        if (s.length>2) return false;
        if (str.length()<3 || str.length() >15) return false;
        return true;
    }

    public  boolean validAddress(String str)
    {
        if (str.length()<8 || str.length() >30) return false;
        return true;
    }

    public  boolean validTelefon(String tel)
    {
        String[] s = tel.split("[\\p{Punct}\\s]+");
        if (tel.charAt(0) == '+' && s.length == 2 ) return true;
        if (tel.charAt(0) != '0')return false;
        if (s.length != 1) return false;
        return true;
    }
}
