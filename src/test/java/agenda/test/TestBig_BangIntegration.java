package agenda.test;


import agenda.exceptions.InvalidFormatException;
import agenda.model.base.Activity;
import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryActivityMock;
import agenda.model.repository.classes.RepositoryContactMock;
import agenda.model.repository.classes.RepositoryUserFile;
import agenda.model.repository.interfaces.RepositoryActivity;
import agenda.model.repository.interfaces.RepositoryContact;
import agenda.model.repository.interfaces.RepositoryUser;
import agenda.utils.Constants;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestBig_BangIntegration {
    private Activity act;
    private RepositoryContact repC;
    private RepositoryActivity repA;


    @Before
    public void setUp() throws Exception {
        repC = new RepositoryContactMock();
        repA = new RepositoryActivityMock();
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();


    @Test
    public void testA() throws InvalidFormatException {
        //testare unitara A (A-invalid)
        //lungime nume inafara intervalului 3..15 de caractere (limita inferioara)
        expectedEx.expect(InvalidFormatException.class);
        expectedEx.expectMessage(Constants.invalidNume);
        Contact contact = new Contact("AAAAAAAAAAAAAAAA", "Str. Margelean", "072627282833");//nume.len>15
        repC.addContact(contact);
    }

    @Test
    public void testB() throws  Exception{
        //testare unitara B (B-valid)
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");

        List<Contact> contacts = new ArrayList<Contact>();
        contacts.add(new Contact("Name1", "address1", "+4071122334455"));

        for (Activity a : repA.getActivities())
            repA.removeActivity(a);

        act = new Activity("name1",
                df.parse("03/20/2013 12:00"),
                df.parse("03/20/2013 13:00"),
                contacts,
                "Lunch");
        repA.addActivity(act);

        assertTrue(1 == repA.count());
        repA.saveActivities();
    }

    @Test
    public void testC() throws Exception {
        //testare unitara C (C-valid)

        List<Contact> contacts = new ArrayList<Contact>();
        contacts.add(new Contact("Name1", "address1", "+4071122334455"));

        for (Activity act : repA.getActivities())
            repA.removeActivity(act);

        Calendar c = Calendar.getInstance();
        c.set(2013, 3 - 1, 20, 12, 00);
        Date start = c.getTime();

        c.set(2013, 3 - 1, 20, 12, 30);
        Date end = c.getTime();

        Activity act = new Activity("name2", start, end,
                contacts, "description2");

        repA.addActivity(act);

        c.set(2013, 3 - 1, 20);
        List<Activity> result = repA.activitiesOnDate(act.getName(), c.getTime());
        assertTrue(result.size() == 1);
    }

    @Test
    public void testCombinare() throws InvalidFormatException {
        //|P -> B -> C -> A| B-valid; C-valid; A-invalid;
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");

        List<Contact> contacts = new ArrayList<Contact>();
        contacts.add(new Contact("Name1", "address1", "+4071122334455"));

        for (Activity act : repA.getActivities())
            repA.removeActivity(act);

        Calendar c = Calendar.getInstance();
        c.set(2013, 3 - 1, 20, 12, 00);
        Date start = c.getTime();

        c.set(2013, 3 - 1, 20, 12, 30);
        Date end = c.getTime();

        Activity act = new Activity("nume1", start, end,
                contacts, "description2");

        try {
            repA.addActivity(act);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //B
        assertTrue(1 == repA.count());
        //C
        c.set(2013, 3 - 1, 20);
        List<Activity> result = repA.activitiesOnDate(act.getName(), c.getTime());
        assertTrue(result.size() == 1);
        //A
        expectedEx.expect(InvalidFormatException.class);
        expectedEx.expectMessage(Constants.invalidNume);
        Contact contact = new Contact("AAAAAAAAAAAAAAAA", "Str. Margelean", "072627282833");//nume.len>15
        repC.addContact(contact);
    }
}
