package agenda.model.base;

import agenda.exceptions.InvalidFormatException;
import agenda.model.validator.Validate;
import agenda.utils.Constants;

public class Contact {
	private String Name;
	private String Address;
	private String Telefon;
	
	public Contact(){
		Name = "";
		Address = "";
		Telefon = "";
	}
	
	public Contact(String name, String address, String telefon) throws InvalidFormatException{
		if (!new Validate().validTelefon(telefon)) throw new InvalidFormatException(Constants.invalidTelefon);
		if (!new Validate().validName(name)) throw new InvalidFormatException(Constants.invalidNume);
		if (!new Validate().validAddress(address)) throw new InvalidFormatException(Constants.invalidAddress);
		Name = name;
		Address = address;
		Telefon = telefon;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) throws InvalidFormatException {
		if (!new Validate().validName(name)) throw new InvalidFormatException(Constants.invalidNume);
		Name = name;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) throws InvalidFormatException {
		if (!new Validate().validAddress(address)) throw new InvalidFormatException(Constants.invalidAddress);
		Address = address;
	}

	public String getTelefon() {
		return Telefon;
	}

	public void setTelefon(String telefon) throws InvalidFormatException {
		if (!new Validate().validTelefon(telefon)) throw new InvalidFormatException(Constants.invalidTelefon);
		Telefon = telefon;
	}

	public static Contact fromString(String str, String delim) throws InvalidFormatException
	{
		String[] s = str.split(delim);
		if (s.length!=4) throw new InvalidFormatException(Constants.invalidData);
		if (!new Validate().validTelefon(s[2])) throw new InvalidFormatException(Constants.invalidTelefon);
		if (!new Validate().validName(s[0])) throw new InvalidFormatException(Constants.invalidNume);
		if (!new Validate().validAddress(s[1])) throw new InvalidFormatException(Constants.invalidAddress);
		
		return new Contact(s[0], s[1], s[2]);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(Name);
		sb.append("#");
		sb.append(Address);
		sb.append("#");
		sb.append(Telefon);
		sb.append("#");
		return sb.toString();
	}

	@Override
	public boolean equals(Object obj) {
		if (! (obj instanceof Contact)) return false;
		Contact o = (Contact)obj;
		if (Name.equals(o.Name) && Address.equals(o.Address) &&
				Telefon.equals(o.Telefon))
			return true;
		return false;
	}
	
}
