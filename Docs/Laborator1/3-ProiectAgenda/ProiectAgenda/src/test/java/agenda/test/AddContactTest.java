package agenda.test;

import static org.junit.Assert.*;

import agenda.utils.Constants;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import agenda.exceptions.InvalidFormatException;

import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryContactMock;
import agenda.model.repository.interfaces.RepositoryContact;
import org.junit.rules.ExpectedException;


public class AddContactTest {

	private Contact con;
	private RepositoryContact rep;

	@Rule
	public ExpectedException expectedEx = ExpectedException.none();
	
	@Before
	public void setUp() throws Exception {
		rep = new RepositoryContactMock();
	}
	
	@Test
	public void testCase1()
	{
		try {
			con = new Contact("name", "address1", "+4071122334455");
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
		//int n = rep.count();
		rep.addContact(con);
		for(Contact c : rep.getContacts())
			if (c.equals(con))
			{
				assertTrue(true);
				break;
			}
		//assertTrue(n+1 == rep.count());
	}
	
	@Test
	public void testCase2()
	{
		try{
			rep.addContact((Contact) new Object());
		}
		catch(Exception e)
		{
			assertTrue(true);
		}	
	}
	
	@Test
	public void testCase3()
	{
		for(Contact c : rep.getContacts())
			rep.removeContact(c);
		
		try {
			con = new Contact("name", "address1", "+071122334455");
			rep.addContact(con);
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
		int n  = rep.count();
		if (n == 1) 
			if (con.equals(rep.getContacts().get(0))) assertTrue(true);
			else assertTrue(false);
		else assertTrue(false);
	}

	@Test
	public void test1() throws InvalidFormatException {
		Contact contact = new Contact("Ion", "address1", "072627282833");
		rep.addContact(contact);
		assertEquals(4, rep.getContacts().size());
	}

	@Test
	public void test2() throws InvalidFormatException {
		Contact contact = new Contact("IonutAmfimov", "Str. Margelean", "072627282833");
		rep.addContact(contact);
		assertEquals(4, rep.getContacts().size());
	}

	@Test
	public void test3() throws InvalidFormatException {
		expectedEx.expect(InvalidFormatException.class);
		expectedEx.expectMessage(Constants.invalidNume);
		Contact contact = new Contact("Io", "Str. Margelean", "072627282833"); //nume<2
		rep.addContact(contact);
	}

	@Test
	public void test4() throws InvalidFormatException {
		expectedEx.expect(InvalidFormatException.class);
		expectedEx.expectMessage(Constants.invalidNume);
		Contact contact = new Contact("Andrei Ionut Marian", "Str. Margelean", "072627282833");//nume has 2 words
		rep.addContact(contact);
	}

	@Test
	public void test5() throws InvalidFormatException {
		expectedEx.expect(InvalidFormatException.class);
		expectedEx.expectMessage(Constants.invalidNume);
		Contact contact = new Contact("AAAAAAAAAAAAAAAA", "Str. Margelean", "072627282833");//nume.len>15
		rep.addContact(contact);
	}

	@Test
	public void test6() throws InvalidFormatException {
		expectedEx.expect(InvalidFormatException.class);
		expectedEx.expectMessage(Constants.invalidAddress);
		Contact contact = new Contact("Ion", "Str.", "072627282833");//address.len<8
		rep.addContact(contact);
	}

	@Test
	public void test7() throws InvalidFormatException {
		expectedEx.expect(InvalidFormatException.class);
		expectedEx.expectMessage(Constants.invalidAddress);
		Contact contact = new Contact("Ion", "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "072627282833");//address.len>30
		rep.addContact(contact);
	}

	@Test
	public void test8() throws InvalidFormatException {
		expectedEx.expect(InvalidFormatException.class);
		expectedEx.expectMessage(Constants.invalidNume);
		Contact contact = new Contact("", "Str. Margelean", "072627282833");//nume is ""
		rep.addContact(contact);
	}

	@Test
	public void test9() throws InvalidFormatException {
		Contact contact = new Contact("IonIonIonIonIon", "address1", "072627282833");
		rep.addContact(contact);
		assertEquals(4, rep.getContacts().size());
	}

	@Test
	public void test10() throws InvalidFormatException {
		Contact contact = new Contact("Ana", "address1", "072627282833");
		rep.addContact(contact);
		assertEquals(4, rep.getContacts().size());
	}

	@Test
	public void test11() throws InvalidFormatException {
		expectedEx.expect(InvalidFormatException.class);
		expectedEx.expectMessage(Constants.invalidAddress);
		Contact contact = new Contact("Ion", "", "072627282833");//address is ""
		rep.addContact(contact);
	}

	@Test
	public void test12() throws InvalidFormatException {
		Contact contact = new Contact("Ion", "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "072627282833");//address.len =30
		rep.addContact(contact);
		assertEquals(4, rep.getContacts().size());
	}

}
