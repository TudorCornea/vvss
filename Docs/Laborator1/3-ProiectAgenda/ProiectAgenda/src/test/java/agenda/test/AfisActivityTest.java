package agenda.test;

import static org.junit.Assert.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import agenda.model.base.Activity;
import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryActivityFile;
import agenda.model.repository.classes.RepositoryActivityMock;
import agenda.model.repository.classes.RepositoryContactFile;
import agenda.model.repository.classes.RepositoryContactMock;
import agenda.model.repository.interfaces.RepositoryActivity;
import agenda.model.repository.interfaces.RepositoryContact;

import org.junit.Before;
import org.junit.Test;

public class AfisActivityTest {
	private Activity act;
	RepositoryActivity rep;
	private RepositoryContact repC;
	private RepositoryActivity repA;

	@Before
	public void setUp() throws Exception {
		RepositoryContact repcon = new RepositoryContactFile();
		rep = new RepositoryActivityFile(repcon);
		repC = new RepositoryContactMock();
		repA = new RepositoryActivityMock();
	}

	@Test
	public void testCase1() throws Exception {
		for (Activity act : rep.getActivities())
			rep.removeActivity(act);

		Calendar c = Calendar.getInstance();
		c.set(2013, 3 - 1, 20, 12, 00);
		Date start = c.getTime();

		c.set(2013, 3 - 1, 20, 12, 30);
		Date end = c.getTime();

		Activity act = new Activity("name1", start, end,
				new LinkedList<Contact>(), "description2");

		rep.addActivity(act);

		c.set(2013, 3 - 1, 20);

		List<Activity> result = rep.activitiesOnDate("name1", c.getTime());
		assertTrue(result.size() == 1);
	}

	@Test
	public void testCase2() throws Exception {
		for (Activity act : rep.getActivities())
			rep.removeActivity(act);

		Calendar c = Calendar.getInstance();
		c.set(2013, 3 - 1, 20, 12, 00);
		Date start = c.getTime();

		c.set(2013, 3 - 1, 20, 12, 30);
		Date end = c.getTime();

		Activity act = new Activity("name1", start, end,
				new LinkedList<Contact>(), "description2");

		rep.addActivity(act);

		c.set(2013, 3 - 1, 20);
		try {
			rep.activitiesOnDate(((Object) 1).toString(), c.getTime());
		} catch (Exception e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testCase3() {
		for (Activity act : rep.getActivities())
			rep.removeActivity(act);

		try {
			rep.activitiesOnDate("name1", (Date)(Object)"ASD");
		} catch (Exception e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testCase4() {
		for (Activity act : rep.getActivities())
			rep.removeActivity(act);

		try {
			rep.addActivity((Activity)(Object)1);
			
			Calendar c = Calendar.getInstance();
			c.set(2013, 3 - 1, 20);
			rep.activitiesOnDate("name1", c.getTime());
		} catch (Exception e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testCase5() {
		for (Activity act : rep.getActivities())
			rep.removeActivity(act);
	
		Calendar c = Calendar.getInstance();
		c.set(2013, 3 - 1, 20);
		List<Activity> result = rep.activitiesOnDate("name1", c.getTime());
		
		assertTrue(result.size() == 0);
	}

	@Test
	public void testF03Valid() throws Exception {
		//valid

		List<Contact> contacts = new ArrayList<Contact>();
		contacts.add(new Contact("Name1", "address2", "+4071122334455"));

		for (Activity act : repA.getActivities())
			repA.removeActivity(act);

		Calendar c = Calendar.getInstance();
		c.set(2013, 3 - 1, 20, 12, 00);
		Date start = c.getTime();

		c.set(2013, 3 - 1, 20, 12, 30);
		Date end = c.getTime();

		Activity act = new Activity("name2", start, end,
				contacts, "description2");

		repA.addActivity(act);

		c.set(2013, 3 - 1, 20);
		List<Activity> result = repA.activitiesOnDate(act.getName(), c.getTime());
		assertTrue(result.size() == 1);
	}


	@Test
	public void testF03Invalid() throws Exception {
		//invalid
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		for (Activity act : rep.getActivities())
			rep.removeActivity(act);

		act = new Activity("name1",
				df.parse("04/20/2013 12:00"),
				df.parse("04/20/2013 14:00"),
				null,
				"Lunch break");
		rep.addActivity(act);

		Calendar c = Calendar.getInstance();
		c.set(2013, 3 - 1, 20);
		List<Activity> result = rep.activitiesOnDate("name1", c.getTime());

		assertTrue(result.size() == 0);
	}
}
