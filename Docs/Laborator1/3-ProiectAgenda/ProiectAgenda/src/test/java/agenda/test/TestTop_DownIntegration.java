package agenda.test;


import agenda.exceptions.InvalidFormatException;
import agenda.model.base.Activity;
import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryActivityMock;
import agenda.model.repository.classes.RepositoryContactMock;
import agenda.model.repository.interfaces.RepositoryActivity;
import agenda.model.repository.interfaces.RepositoryContact;
import agenda.utils.Constants;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestTop_DownIntegration {
    private Activity act;
    private RepositoryContact repC;
    private RepositoryActivity repA;


    @Before
    public void setUp() throws Exception {
        repC = new RepositoryContactMock();
        repA = new RepositoryActivityMock();
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testA() throws InvalidFormatException {
        //testare unitara pentru modulul A (A-valid)
        Contact contact = new Contact("Ion", "address1", "072627282833");
        repC.addContact(contact);
        assertEquals(4, repC.getContacts().size());
        assertEquals(repC.getContacts().get(3).getName(),"Ion");
        assertEquals(repC.getContacts().get(3).getAddress(),"address1");
    }

    @Test
    public void testB() throws Exception {
        //testare de integrare a modulului B
        //P->B->A A-valid B-invalid
        //B
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        Activity activity = new Activity("name1",
                df.parse("02/02/2010 12:00"),
                df.parse("01/01/2010 12:00"),
                null,
                "Lunch breakk");

        repA.addActivity(activity);
        assertFalse(repA.addActivity(activity));
        assertEquals(0, repA.getActivities().size());
        //A
        Contact contact = new Contact("Ion", "address1", "072627282833");
        repC.addContact(contact);
        assertEquals(4, repC.getContacts().size());
    }

    @Test
    public void testCombinate() throws Exception {
        //testare de integrare a modulului C;
        //P->B->A->C B-invalid A-valid C-valid
        //B
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        Activity activity = new Activity("name1",
                df.parse("02/02/2010 12:00"),
                df.parse("01/01/2010 12:00"),
                null,
                "Lunch breakk");

        repA.addActivity(activity);
        assertFalse(repA.addActivity(activity));
        assertEquals(0, repA.getActivities().size());

        //A
        Contact contact = new Contact("Ionut", "address1", "072627282833");
        repC.addContact(contact);
        assertEquals(4, repC.getContacts().size());

        LinkedList<Contact> linkedlist = new LinkedList<Contact>();
        linkedlist.add(contact);
        for (Activity a : repA.getActivities())
            repA.removeActivity(a);

        Calendar c = Calendar.getInstance();
        c.set(2013, 3 - 1, 20, 12, 00);
        Date start = c.getTime();

        c.set(2013, 3 - 1, 20, 12, 30);
        Date end = c.getTime();

        Activity act = new Activity("name2", start, end,
                linkedlist, "description2");

        repA.addActivity(act);

        //C
        c.set(2013, 3 - 1, 20);
        List<Activity> result = repA.activitiesOnDate(act.getName(), c.getTime());
        assertTrue(result.size() == 1);
    }
}
